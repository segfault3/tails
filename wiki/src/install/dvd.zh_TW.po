# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails l10n\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2017-11-09 18:59+0000\n"
"PO-Revision-Date: 2018-11-02 16:55+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: Tails Chinese translators <jxt@twngo.xyz>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Burning Tails on a DVD\"]]\n"
msgstr "[[!meta title=\"將 Tails 燒錄到 DVD 光碟片\"]]\n"

#. type: Plain text
msgid "For detailed instructions, refer to the Ubuntu documentation:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[Burning from Windows](https://help.ubuntu.com/community/"
"BurningIsoHowto#Burning_from_Windows)"
msgstr "[利用 Windows 燒錄](https://wiki.ubuntuusers.de/Ubuntu-CD/)"

#. type: Bullet: '  - '
msgid ""
"[Burning from Ubuntu](https://help.ubuntu.com/community/"
"BurningIsoHowto#Burning_from_Ubuntu)"
msgstr ""
"[利用 Ubuntu 燒錄](https://help.ubuntu.com/community/"
"BurningIsoHowto#Burning_from_Ubuntu)"

#. type: Bullet: '  - '
msgid ""
"[Burning from macOS](https://help.ubuntu.com/community/"
"BurningIsoHowto#Burning_from_Mac_OS_X)"
msgstr ""
