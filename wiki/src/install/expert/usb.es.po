# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-02-21 12:48+0000\n"
"PO-Revision-Date: 2019-07-21 13:29+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"install-expert-usb/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install from Debian, Ubuntu, or Mint using the command line and GnuPG\"]]\n"
msgstr "[[!meta title=\"Instalar desde Debian, Ubuntu, o Mint usando la linea de comandos y GnuPG\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
msgid "Start in Debian, Ubuntu, or Linux Mint."
msgstr "Iniciar en Debian, Ubuntu, o Linux Mint."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"verify-key\">Verify the Tails signing key</h1>\n"
msgstr "<h1 id=\"verify-key\">Verifica la llave de firmado de Tails</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>If you already certified the Tails signing key with your own key, you\n"
#| "can skip this step and start [[downloading and verifying the ISO\n"
#| "image|usb#download]].</p>\n"
msgid ""
"<p>If you already certified the Tails signing key with your own key, you\n"
"can skip this step and start [[downloading and verifying the USB\n"
"image|usb#download]].</p>\n"
msgstr ""
"<p>Si ya certificaste la llave de firmado de Tails con tu propia llave, puedes\n"
"saltarte este paso e iniciar la [[descarga y verificación de la imagen\n"
"ISO|usb#download]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "In this step, you will download and verify the *Tails signing key* which "
#| "is the OpenPGP key that is used to cryptographically sign the Tails ISO "
#| "image."
msgid ""
"In this step, you will download and verify the *Tails signing key* which is "
"the OpenPGP key that is used to cryptographically sign the Tails USB image."
msgstr ""
"En este paso, vas a descargar y verificar la *llave de firmado de Tails** "
"que es la llave OpenPGP que es usada para firmar la imagen ISO de Tails "
"criptográficamente."

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To follow these instructions you need to have your own OpenPGP\n"
"key.</p>\n"
msgstr ""
"<p>Para seguir estas instrucciones necesitas tener tu propia llave\n"
"OpenPGP.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To learn how to create yourself an OpenPGP key, see\n"
"<a href=\"https://help.riseup.net/en/security/message-security/openpgp/gpg-keys\">Managing\n"
"OpenPGP Keys</a> by <em>Riseup</em>.</p>\n"
msgstr ""
"<p>Para aprender como crear tu propia llave OpenPGP, lee\n"
"<a href=\"https://help.riseup.net/en/security/message-security/openpgp/gpg-keys\">Managing\n"
"OpenPGP Keys</a> por <em>Riseup</em>.</p>\n"

#. type: Plain text
msgid ""
"This verification technique uses the OpenPGP Web of Trust and the "
"certification made by official Debian developers on the Tails signing key. "
"[[Learn more about the OpenPGP Web of Trust|install/download#openpgp]]."
msgstr ""
"Esta técnica de verificación utiliza la Red de Confianza de OpenPGP y la "
"certificación hecha por desarrolladores oficiales de Debian en la llave de "
"firmado de Tails. [[Aprende más sobre la Red de Confianza de OpenPGP|install/"
"download#openpgp]]."

#. type: Bullet: '1. '
msgid ""
"Import the Tails signing key in your <span class=\"application\">GnuPG</"
"span> keyring:"
msgstr ""
"Importar la llave de firmado de Tails en tu deposito de claves de <span "
"class=\"application\">GnuPG</span>:"

#. type: Plain text
#, no-wrap
msgid ""
"       wget https://tails.boum.org/tails-signing.key\n"
"       gpg --import < tails-signing.key\n"
msgstr ""
"       wget https://tails.boum.org/tails-signing.key\n"
"       gpg --import < tails-signing.key\n"

#. type: Bullet: '1. '
msgid ""
"Install the Debian keyring. It contains the OpenPGP keys of all Debian "
"developers:"
msgstr ""
"Instala el deposito de claves de Debian. Contiene las llaves OpenPGP de "
"todos los desarrolladores de Debian:"

#. type: Plain text
#, no-wrap
msgid "       sudo apt install debian-keyring\n"
msgstr "       sudo apt install debian-keyring\n"

#. type: Bullet: '1. '
msgid ""
"Import the OpenPGP key of [[!wikipedia Stefano_Zacchiroli]], a former Debian "
"Project Leader, from the Debian keyring into your keyring:"
msgstr ""
"Importa la llave OpenPGP de [[!wikipedia_es Stefano_Zacchiroli]], un antiguo "
"Lider de Proyecto de Debian, del deposito de claves de Debian a tu deposito "
"de claves:"

#. type: Plain text
#, no-wrap
msgid "       gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export zack@upsilon.cc | gpg --import\n"
msgstr "       gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export zack@upsilon.cc | gpg --import\n"

#. type: Bullet: '1. '
msgid "Verify the certifications made on the Tails signing key:"
msgstr "Verifica las certificaciones hechas en la llave de firmado de Tails:"

#. type: Plain text
#, no-wrap
msgid "       gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr "       gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid "   In the output of this command, look for the following line:\n"
msgstr "   En el resultado de este comando, busca la siguiente linea:\n"

#. type: Plain text
#, no-wrap
msgid "       sig! 0x9C31503C6D866396 2015-02-03  Stefano Zacchiroli <zack@upsilon.cc>\n"
msgstr "       sig! 0x9C31503C6D866396 2015-02-03  Stefano Zacchiroli <zack@upsilon.cc>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   Here, <code>sig!</code>, with an exclamation mark, means that Stefano\n"
"   Zacchiroli verified and certified the Tails signing key with his key.\n"
msgstr ""
"   Aquí, <code>sig!</code>, con un signo de exclamación, significa que Stefano\n"
"   Zacchiroli verificó y certificó la llave de firmado de Tails con su llave.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   It is also possible to verify the certifications made by other\n"
"   people. Their name and email address appear in the list of\n"
"   certification if you have their key in your keyring.\n"
msgstr ""
"   También es posible verificar las certificaciones hechas por otra\n"
"   gente. Su nombre y dirección de email aparece en la lista de\n"
"   certificación si tienes su llave en tu deposito de claves.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If the verification of the certification failed, then you might\n"
"   have downloaded a malicious version of the Tails signing key or our\n"
"   instructions might be outdated.\n"
"   Please [[get in touch with us|support/talk]].</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"caution\">\n"
"   <p>Si la verificación de la certificación falla, significa que puedes\n"
"   haber descargado una versión maliciosa de la llave de firmado de Tails o nuestras\n"
"   instrucciones pueden estar desactualizadas.\n"
"   Por favor [[ponte en contacto con nosotros|support/talk]].</p>\n"
"   </div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"tip\">\n"
"   <p>The line `175 signatures not checked due to missing keys` or similar\n"
"   refers to the certifications (also called *signatures*) made by other public\n"
"   keys that are not in your keyring. This is not a problem.</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"tip\">\n"
"   <p>La frase `175 signatures not checked due to missing keys` o similar\n"
"   se refiere a las certificaciones (también llamadas *firmas*) hechas por otras\n"
"   llaves que no están en tu llavero. Esto no es un problema</p>\n"
"   </div>\n"

#. type: Bullet: '1. '
msgid "Certify the Tails signing key with your own key:"
msgstr "Certifica la llave de firmado de Tails con tu propia llave:"

#. type: Plain text
#, no-wrap
msgid ""
"   a. To make a non-exportable certification that will never be shared\n"
"      with others:\n"
msgstr ""
"   a. Crear una certificación no exportable que nunca será compartida\n"
"      con nadie:\n"

#. type: Plain text
#, no-wrap
msgid "          gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr "          gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid ""
"   b. To make an exportable certification of the Tails signing\n"
"      key and publish it on the public key servers:\n"
msgstr ""
"   b. Crear una certificación exportable de la llave de firma de Tails\n"
"      y publicarla en los servidores de llaves:\n"

#. type: Plain text
#, no-wrap
msgid ""
"          gpg --sign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
"          gpg --send-keys A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
msgstr ""
"          gpg --sign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"
"          gpg --send-keys A490D0F4D311A4153E2BB7CADBB802B258ACD84F\n"

#. type: Plain text
#, no-wrap
msgid ""
"      Doing so allows people who verified\n"
"      your key to verify your certification and, as a consequence, build\n"
"      more trust in the Tails signing key.\n"
msgstr ""
"      Hacer esto permite a la gente que haya verificado\n"
"      tu llave verificar a su vez la certificación, y como consecuencia, construir\n"
"      más confianza en la llave de firma de Tails.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"download\"></a>\n"
msgstr "<a id=\"download\"></a>\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h1 id=\"download-verify\">Download and verify the ISO image</h1>\n"
msgid "<h1 id=\"download-verify\">Download and verify the USB image</h1>\n"
msgstr "<h1 id=\"download-verify\">Descargar y verificar la imagen ISO</h1>\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "In this step, you will download the latest Tails ISO image and verify it "
#| "using the Tails signing key."
msgid ""
"In this step, you will download the latest Tails USB image and verify it "
"using the Tails signing key."
msgstr ""
"En este paso, descargarás la última imagen ISO de Tails y la verificarás "
"usando la llave de firmado de Tails."

#. type: Bullet: '1. '
#, fuzzy
#| msgid "Download the ISO image:"
msgid "Download the USB image:"
msgstr "Descargar la imagen ISO:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">wget --continue [[!inline pages=\"inc/stable_amd64_iso_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"pre\">wget --continue [[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">wget --continue [[!inline pages=\"inc/stable_amd64_iso_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid "Download the signature of the ISO image:"
msgid "Download the signature of the USB image:"
msgstr "Descargar la firma de la imagen ISO:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">wget [[!inline pages=\"inc/stable_amd64_iso_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"pre\">wget [[!inline pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">wget [[!inline pages=\"inc/stable_amd64_iso_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid "Verify that the ISO image is signed by the Tails signing key:"
msgid "Verify that the USB image is signed by the Tails signing key:"
msgstr ""
"Verificar que la imagen ISO esté firmada por la llave de firmado de Tails:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   The output of this command should be the following:\n"
msgstr "   La salida de este comando debería ser la siguiente:\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   Verify in this output that:\n"
msgstr "   Verifica en esta salida que:\n"

#. type: Bullet: '     - '
msgid "The date of the signature is the same."
msgstr "La fecha de la firma es la misma."

#. type: Bullet: '     - '
msgid ""
"The signature is marked as <code>Good signature</code> since you certified "
"the Tails signing key with your own key."
msgstr ""
"La firma se marcará como <code>Buena firma</code> ya que tú has certificado "
"la llave de firma de Tails con tu propia llave."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a id=\"download\"></a>\n"
msgid "<a id=\"dd\"></a>\n"
msgstr "<a id=\"download\"></a>\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "Install <span class=\"application\">Tails Installer</span>\n"
msgid "Install Tails using <span class=\"command\">dd</span>\n"
msgstr "Instalar <span class=\"application\">Tails Installer</span>\n"

#. type: Bullet: '1. '
msgid ""
"Make sure that the USB stick on which you want to install Tails is unplugged."
msgstr ""

#. type: Bullet: '1. '
msgid "Execute the following command:"
msgstr "Ejecuta el siguiente comando:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command\">ls -1 /dev/sd?</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   It returns a list of the storage devices on the system. For example:\n"
msgstr ""
"   Devuelve una lista de los dispositivos de almacenamiento del sistema. Por "
"ejemplo:\n"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command-output\">/dev/sda</p>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Plug in the USB stick on which you want to install Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Execute again the same command:"
msgstr "Ejecuta otra vez el mismo comando:"

#. type: Plain text
#, no-wrap
msgid "   Your USB stick appears as a new device in the list.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"pre command-output\">/dev/sda\n"
"   /dev/sdb</p>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Take note of the *device name* of your USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   In this example, the device name of the USB stick is\n"
"   <span class=\"code\">/dev/sdb</span>. Yours might be different.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If you are unsure about the device name, you should stop proceeding or\n"
"   <strong>you risk overwriting any hard disk on the system</strong>.</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"caution\">\n"
"<p>Si no estás seguro del nombre del dispositivo deberías parar ahora o\n"
"<strong>te arriesgas a sobreescribir cualquier otro disco duro del "
"sistema</strong>.</p>\n"
"</div>\n"

#. type: Bullet: '1. '
msgid ""
"Execute the following commands to copy the USB image that you downloaded "
"earlier to the USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   Replace:\n"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">tails.img</span> with the path to the "
"USB image"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">device</span> with the device name found "
"in step 5"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command\">dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M && sync</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   You should get something like this:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <p class=\"pre command-example\">dd if=/home/user/tails-amd64-3.12.img of=/dev/sdb bs=16M && sync</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   If no error message is returned, Tails is being copied on the USB\n"
"   stick. The copy takes some time, generally a few minutes.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"note\">\n"
"   <p>If you get a <span class=\"guilabel\">Permission denied</span> error, try\n"
"   adding <code>sudo</code> at the beginning of the command:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <p class=\"pre command\">sudo dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M && sync</p>\n"
"   </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   The installation is complete after the command prompt reappears.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/create_persistence.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/create_persistence.inline.es\" raw=\"yes\" sort=\"age\"]]\n"
