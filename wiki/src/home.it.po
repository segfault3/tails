# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-07-20 16:34+0000\n"
"PO-Revision-Date: 2019-05-24 09:54+0200\n"
"Last-Translator: \n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Welcome to Tails!\"]] [[!meta stylesheet=\"home\" rel="
"\"stylesheet\" title=\"\"]] [[!meta robots=\"noindex\"]] [[!meta script="
"\"home\"]]"
msgstr ""

#. type: Content of: <div>
msgid "[[!inline pages=\"home/donate\" raw=\"yes\" sort=\"age\"]]"
msgstr "[[!inline pages=\"home/donate.it\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div>
msgid "[[!inline pages=\"news\" raw=\"yes\" sort=\"age\"]]"
msgstr "[[!inline pages=\"news.it\" raw=\"yes\" sort=\"age\"]]"

#~ msgid "[[!inline pages=\"home/tor_check\" raw=\"yes\" sort=\"age\"]]"
#~ msgstr "[[!inline pages=\"home/tor_check.it\" raw=\"yes\" sort=\"age\"]]"

#, fuzzy
#~| msgid ""
#~| "<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link="
#~| "\"no\"]] <span>Tor check</span> </a>"
#~ msgid ""
#~ "<a href=\"https://tails.boum.org/install/check/\"> [[!img \"lib/onion.png"
#~ "\" link=\"no\"]] <span>Tor check</span> </a>"
#~ msgstr ""
#~ "<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link="
#~ "\"no\"]] <span>Verifica Tor</span> </a>"
