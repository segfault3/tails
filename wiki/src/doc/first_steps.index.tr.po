# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-09-15 11:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Bullet: '  - '
msgid "[[!traillink Start_Tails|first_steps/start_tails]]"
msgstr ""

#. type: Bullet: '  - '
msgid "[[!traillink Accessibility|first_steps/accessibility]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - [[!traillink Startup_options|first_steps/startup_options]]\n"
"    - [[!traillink Administration_password|first_steps/startup_options/administration_password]]\n"
"    - [[!traillink MAC_address_spoofing|first_steps/startup_options/mac_spoofing]]\n"
"    - [[!traillink Tor_bridge_mode|first_steps/startup_options/bridge_mode]]\n"
"  - [[!traillink Introduction_to_GNOME_and_the_Tails_desktop|first_steps/introduction_to_gnome_and_the_tails_desktop]]\n"
"  - [[!traillink Encrypted_persistence|first_steps/persistence]]\n"
"    - [[!traillink Warnings_about_persistence|first_steps/persistence/warnings]]\n"
"    - [[!traillink Create_&_configure_the_persistent_volume|first_steps/persistence/configure]]\n"
"    - [[!traillink Enable_&_use_the_persistent_volume|first_steps/persistence/use]]\n"
"    - [[!traillink Change_the_passphrase_of_the_persistent_volume|first_steps/persistence/change_passphrase]]\n"
"    - [[!traillink Manually_copy_your_persistent_data_to_a_new_USB_stick|first_steps/persistence/copy]]\n"
"    - [[!traillink Check_the_file_system_of_the_persistent_volume|first_steps/persistence/check_file_system]]\n"
"    - [[!traillink Delete_the_persistent_volume|first_steps/persistence/delete]]\n"
"  - [[!traillink Installing_additional_software|first_steps/additional_software]]\n"
"  - [[!traillink Report_an_error|first_steps/bug_reporting]]\n"
"  - [[!traillink Shutting_down_Tails|first_steps/shutdown]]\n"
"  - [[!traillink Upgrading_a_Tails_USB_stick|first_steps/upgrade]]\n"
"  - Uninstalling Tails or resetting a USB stick\n"
"    using [[!traillink Linux|first_steps/reset/linux]],\n"
"    [[!traillink Windows|first_steps/reset/windows]],\n"
"    or [[!traillink Mac|first_steps/reset/mac]]\n"
msgstr ""
