# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-07-20 16:34+0000\n"
"PO-Revision-Date: 2019-05-24 09:55+0200\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/news/fa/"
">\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid "[[!meta title=\"News\"]] [[!meta robots=\"noindex\"]]"
msgid ""
"[[!meta title=\"Welcome to Tails!\"]] [[!meta stylesheet=\"home\" rel="
"\"stylesheet\" title=\"\"]] [[!meta robots=\"noindex\"]] [[!meta script="
"\"home\"]]"
msgstr "[[!meta title=\"اخبار\"]] [[!meta robots=\"noindex\"]]"

#. type: Content of: <div>
msgid "[[!inline pages=\"home/donate\" raw=\"yes\" sort=\"age\"]]"
msgstr "[[!inline pages=\"home/donate.fa\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div>
msgid "[[!inline pages=\"news\" raw=\"yes\" sort=\"age\"]]"
msgstr "[[!inline pages=\"news.fa\" raw=\"yes\" sort=\"age\"]]"

#~ msgid "[[!inline pages=\"home/tor_check\" raw=\"yes\" sort=\"age\"]]"
#~ msgstr "[[!inline pages=\"home/tor_check.fa\" raw=\"yes\" sort=\"age\"]]"

#, fuzzy
#~| msgid ""
#~| "<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link="
#~| "\"no\"]] <span>Tor check</span> </a>"
#~ msgid ""
#~ "<a href=\"https://tails.boum.org/install/check/\"> [[!img \"lib/onion.png"
#~ "\" link=\"no\"]] <span>Tor check</span> </a>"
#~ msgstr ""
#~ "<a href=\"https://check.torproject.org/\"> [[!img \"lib/onion.png\" link="
#~ "\"no\"]] <span>تست تور</span> </a>"
