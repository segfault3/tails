[[!meta title="Translation platform"]]
[[!toc levels=2]]

Our (website) translation infrastructure has a pretty high barrier for
new translators, especially those who are not familiar with Git and/or
the command line.
Furthermore, the current process makes it hard to add new languages, as
often a team cannot be built easily over a long period of time and a web
interface could nevertheless help keep translations until a new person
arrives.

Corresponding ticket: [[!tails_ticket 10034]]

Choosing a translation web platform
===================================

MUST
----

* provide a usable easy web interface
* be usable from Tor Browser
* automatic pull from main Git repo
* provide a common glossary for each language, easy to use and improve
* allow translators to view, in the correct order, all strings that
  come from the entire page being translated, both in English and in
  the target language
* make it easy to view the translations in context i.e. when translating an entire page, all strings to be translated should only come from this page. translators should be able to view the page in context.
* provide user roles (admin, reviewer, translator)

SHOULD
------

* be "privacy sensitive", i.e. be operated by a non-profit
* allow translators to push translations through Git (so that core
  developers only have to fetch reviewed translations from there)
* provide support for Git standard development branches (devel, stable,
  and testing) but we could also agree upon translation only master
  through this interface
* provide checks for inconsistent translations
* provide feature to write/read comments between translators

MAY
---

* allow translating topic branches without confusing translators,
  causing duplicate/premature work, fragmentation or merge conflicts
  -- e.g. present only new or updated strings in topic branches;
  see <https://mailman.boum.org/pipermail/tails-l10n/2015-March/002102.html>
  for details
* provide a feature to easily see what is new, what needs updating, what are translation priorities
* provide possibility to set up new languages easily
* send email notifications
  - to reviewers whenever new strings have been translated or updated
  - to translators whenever a resource is updated
* respect authorship (different committers?)
* provide statistics about the percentage of translated and fuzzy strings
* Letting translators report about problems in original strings, e.g.
  with a "Report a problem in the original English text" link, that
  e.g. results in an email being sent to -l10n@ or -support-private@.
  If we don't have that, then [[contribute/how/translate]] MUST
  document how to report issues in the original English text.

Setup of our translation platform & integration with our infrastructure
=======================================================================

Documentation
-------------

We have two documentations:

- this one, which presents the current status of our work and all public
  information
- translate-server.git which contains more technical information,
  cronjobs, configs, inner workings of the setup.

Translation web interface
-------------------------

We are testing a [Weblate instance](https://translate.tails.boum.org/)
to see if it fits our requirements. Read [[!tails_ticket 11759]] for
more information.

There are several languages enabled and users can suggest translations
in several languages:

<a href="https://translate.tails.boum.org/engage/tails/?utm_source=widget">
<img src="https://translate.tails.boum.org/widgets/tails/-/multi-red.svg" alt="Translation status" />
</a>

What we plan to do is:

[Schematics of the different Git repos, ikiwiki instances, and their relationships.](https://labs.riseup.net/code/attachments/download/1922/weblate.svg)

[Schematics of the infrastructure such as Git hooks.](https://labs.riseup.net/code/attachments/download/1928/weblate_hooks.svg)

Repository
----------

Currently the repository used by Weblate is built upon the Tails master
repo; the changes generated in translate.lizard are fed back
to Tails master automatically.

There are several languages enabled, some of them with few or not
translations.

You can check out weblate-generated Tails repo with:

    git clone https://translate.tails.boum.org/git/tails/index/

<a id="staging-website"></a>

Staging website
---------------

The [staging website](https://staging.tails.boum.org/) is built regularly.
On top of what our production website has, it includes:

 - all languages available on Weblate, even those that are not enabled
   on our production website yet;

 - all translation suggestions made on Weblate.

This allows:

 - translators to check how the result of their work will look like
   on our website;

 - reviewers to check how translation suggestions look like on the
   website, before validating them.

Updating the repo
-----------------

POT and PO files for enabled languages are built and updated from the
`*.mdwn` files when [[building the wiki|contribute/build/website]].
Then, our translation platform updates the PO files for additional
languages (that are enabled on Weblate but not on the production website),
commits & pushes them, and updates the Weblate database accordingly.

<a id="access-control"></a>

Access control
--------------

### Requirements

- Every translation change must be reviewed by another person before
  it's validated (and thus committed by Weblate and pushed to our
  production website).

  - This requirement must be enforced via technical means, for
    translators that are not particularly trusted (e.g. new user
    accounts). For example, it must be impossible for an attacker to
    pretend to be that second person and validate their own changes,
    simply by creating a second user account.

  - It's acceptable that this requirement is enforced only via social
    rules, and not via technical means, for a set of
    trusted translators.

- We need to be able to bootstrap a new language and give its
  translators sufficient access rights so that they can do their job,
  even without anyone at Tails personally knowing any of them.

### Currently implemented proposal

- In Weblate lingo, we use the [dedicated
  reviewers](https://docs.weblate.org/en/latest/workflows.html#dedicated-reviewers)
  workflow: it's the only one that protects us against an adversary
  who's ready to create multiple user accounts.

- When not logged in, a visitor is in the `Guests` group and is
  only allowed to suggest translations.

- Every logged in user is in the `Users` group. Members of this group
  are allowed to suggest translations but not to accept suggestions
  nor to directly save new translations of their own.

- A reviewer, i.e. a member of the `@Review` group in Weblate, is
  allowed to accept suggestions.

  Limitations:

  - Technically, reviewers are also allowed to directly save new
    translations of their own, edit existing translations, and
    accept their own suggestions; we will ask them in our
    documentation to use this privilege sparingly, only to fix
    important and obvious problems.

	Even if we forbid reviewers to accept their own suggestions,
    nothing would prevent them from creating another account, making
    the suggestion from there, and then accepting it with their
    reviewer account.

  - Reviewer status is global to our Weblate instance, and not
    per-language, so technically, a reviewer can very well accept
    suggestions for a language they don't speak. We will ask them in
    our documentation to _not_ do that, except to fix important and
    obvious problems that don't require knowledge of that language
    (for example, broken syntax for ikiwiki directives).

	If this ever causes actual problems, this could be fixed with
    [group-based access
    control](https://docs.weblate.org/en/weblate-2.20/admin/access.html#groupacl)

- How one gets reviewer status:

  - We will port to Weblate semantics the pre-existing trust
    relationship we already have towards translation teams that have
    been using Git so far: they all become reviewers.

	To this aim, we will ask them to create an account on Weblate
	and tell us what their user name is.

  - One can request reviewer status to Weblate administrators, who
    will accept this request if, and only if, a sufficient amount of
    work was done by the requesting translator (this can be checked on
    the user's page, e.g.
    [intrigeri's](https://translate.tails.boum.org/user/intrigeri/).
	In other words, we use proof-of-work to increase the cost of attacks.

- Bootstrapping a new language

  As a result of this access control setup, translators for a new
  language can only make suggestions until they have done a sufficient
  amount of work and two of them are granted reviewer status. In the
  meantime, they can see the output of their work on the [[staging
  website|blueprint/translation_platform#staging-website]].

  Pending questions:
  
  - Is the resulting UX good enough? Would it help if we allowed them
    to vote up suggestions, even if this does not result in the
    suggestion to be accepted as a validated translation?

Weblate installation and maintenance - a hybrid approach
--------------------------------------------------------

The Tails infrastructure uses Puppet to make it easier to enforce and replicate system configuration, and usually relies on Debian packages to ensure stability of the system. But the effort to maintain a stable system somehow conflicts with installing and maintaining Weblate, a Python web application, which requires using up-to-date versions of Weblate itself and of its dependencies.

Having that in mind, and taking into account that we already started using Docker to replicate the translation server environment to experiment with upgrading and running an up-to-date version of Weblate, it can be a good trade-off to use Puppet to provide an environment to run Docker, and to use a Docker container to actually run an up-to-date Weblate installation.

From the present state of the Docker image, which currently uses (slightly modified/updated) Puppet code to configure the environment and then sets up Weblate, the following steps could be taken to achieve a new service configuration as described above:

* Move the database to a separate Docker service.
* Remove all Puppet code from the Docker image: inherit from the simplest possible Docker image and setup a Weblate Docker image with all needed dependencies.
* Modify the Puppet code to account for setting up an environment that has Docker installed and that runs the Weblate Docker image.
* Set up persistence for the Weblate git repository and configuration.
* Set up persistence and backups for the database service.
* Update the Puppet code to run tmserver (if/when it's needed -- latest Weblate accounts for basic suggestions using its own database).

After that, we should have a clear separation between stable infrastructure maintenance using Debian+Puppet in one side and up-to-date Weblate application deployment using Docker in the other side. The Docker image would have to be constantly maintained to account for Weblate upgrades, but that should be easier cleaner than deploying Weblate directly in the server.
