# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails 2.2.1\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-08-20 15:43+0200\n"
"PO-Revision-Date: 2018-04-27 10:41+0000\n"
"Last-Translator: Tails translators <tails-l10n@boum.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Manually upgrade from another Tails\"]] [[!meta robots="
#| "\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" "
#| "title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel="
#| "\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/"
#| "overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/"
#| "inc/stylesheets/upgrade-clone\" rel=\"stylesheet\" title=\"\"]] [[!inline "
#| "pages=\"install/inc/tails-installation-assistant.inline\" raw=\"yes\" "
#| "sort=\"age\"]] [[!inline pages=\"install/inc/overview\" raw=\"yes\" sort="
#| "\"age\"]] [["
msgid ""
"[[!meta title=\"Manually upgrade from another Tails\"]] [[!meta robots="
"\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title="
"\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/"
"overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/"
"stylesheets/upgrade-clone\" rel=\"stylesheet\" title=\"\"]] [[!inline pages="
"\"install/inc/overview\" raw=\"yes\" sort=\"age\"]] [["
msgstr ""
"[[!meta title=\"Atualizar manualmente a partir de outro Tails\"]] [[!meta "
"robots=\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" "
"title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/stylesheets/"
"overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"install/inc/"
"stylesheets/upgrade-clone\" rel=\"stylesheet\" title=\"\"]] [[!inline pages="
"\"install/inc/tails-installation-assistant.inline.pt\" raw=\"yes\" sort=\"age"
"\"]][[!inline pages=\"install/inc/overview.pt\" raw=\"yes\" sort=\"age\"]] [["

#. type: Content of: <div><div>
msgid "Let's go!"
msgstr "Vamos lá!"

#. type: Content of: outside any tag (error?)
msgid "|upgrade/clone]]"
msgstr "|upgrade/clone]]"

#~ msgid "Manually upgrade from <strong>another Tails</strong>"
#~ msgstr "Atualizar manualmente a partir de <strong>outro Tails</strong>"

#~ msgid ""
#~ "[[!inline pages=\"install/inc/overview\" raw=\"yes\" sort=\"age\"]] [["
#~ msgstr ""
#~ "[[!inline pages=\"install/inc/overview\" raw=\"yes\" sort=\"age\"]] [["
