# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# E <ehuseynzade@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-30 18:53+0200\n"
"PO-Revision-Date: 2014-12-30 17:30+0000\n"
"Last-Translator: E <ehuseynzade@gmail.com>\n"
"Language-Team: Azerbaijani (http://www.transifex.com/projects/p/torproject/"
"language/az/)\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor hazırdır"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "İndi internetə qoşula bilərsən."

#: config/chroot_local-includes/etc/whisperback/config.py:69
#, fuzzy, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Problemini həll etmək üçün bizə kömək et!</h1>\n"
"<p><a href=\"%s\">Bizim problem haqqında xəbər qaydalarımızı</a> oxu.</p>\n"
"<p><strong>Lazım olandan artıq məlumat daxil\n"
"etmə!</strong></p>\n"
"<h2>Bizə təqdim edilən email ünvan</h2>\n"
"<p>Şəxsi məlumatlarının çox kiçik hissəsini Tails-ə\n"
"demək sənin üçün problem deyilsə, problemini daha\n"
"yaxşı həll etməyimiz üçün email ünvanını bizə yaza bilərsən.\n"
"İctimai PGP-in daxil edilməsi əlavə olaraq gələcək \n"
"kommunikasiyamızı şifrələməyə imkan verir.</p>\n"
"<p>Bu cavabı görə bilən hər kəs sənin Tails istifadəçisi\n"
"olduğun nəticəsinə gələcək. \n"
"İnternet və ya email təminatçılarına nə qədər inandığınla\n"
"maraqlanmaq vaxtıdır?</p>\n"

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:51
msgid ""
"You can install additional software automatically from your persistent "
"storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:77
msgid ""
"The following software is installed automatically from your persistent "
"storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:135
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:173
msgid ""
"To add more, install some software using <a href=\"synaptic.desktop"
"\">Synaptic Package Manager</a> or <a href=\"org.gnome.Terminal.desktop"
"\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:154
msgid "_Create persistent storage"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:57
msgid "Persistence is disabled for Electrum"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:59
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:60
msgid "Do you want to start Electrum anyway?"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:63
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Başlat"

#: config/chroot_local-includes/usr/local/bin/electrum:64
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Çıxış"

#: config/chroot_local-includes/usr/local/bin/keepassx:17
#, sh-format
msgid ""
"<b><big>Do you want to rename your <i>KeePassX</i> database?</big></b>\n"
"\n"
"You have a <i>KeePassX</i> database in your <i>Persistent</i> folder:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Renaming it to <i>keepassx.kdbx</i> would allow <i>KeePassX</i> to open it "
"automatically in the future."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/keepassx:25
msgid "Rename"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/keepassx:26
msgid "Keep current name"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/replace-su-with-sudo:21
msgid "su is disabled. Please use sudo instead."
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:75
msgid "Lock screen"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:79
msgid "Suspend"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:83
msgid "Restart"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:87
msgid "Power Off"
msgstr "Söndür"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Tails Haqqında"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "Amnestik İnkoqnito Canlı Sistemi"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Quruluş məlumatı:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "qeyri mümkün"

#. Translators: Don't translate {details}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:150
#, fuzzy, python-brace-format
msgid ""
"{details} Please check your list of additional software or read the system "
"log to understand the problem."
msgstr ""
"Təkmilləşmə alınmadı. Bunun səbəbi şəbəkə problemi ola bilər. Lütfən, şəbəkə "
"əlaqəni yoxla, Tails-i yenidən başlatmağa cəhd et, və ya problemi daha yaxşı "
"anlamaq üçün sistem girişini oxu."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:155
msgid ""
"Please check your list of additional software or read the system log to "
"understand the problem."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:159
msgid "Show Log"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:159
msgid "Configure"
msgstr ""

#. Translators: Don't translate {beginning} or {last}, they are
#. placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:225
#, python-brace-format
msgid "{beginning} and {last}"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:226
msgid ", "
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:292
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:322
#, fuzzy, python-brace-format
msgid "Add {packages} to your additional software?"
msgstr "Sənin əlaqə proqramın"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:294
msgid ""
"To install it automatically from your persistent storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:296
msgid "Install Every Time"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:297
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:328
msgid "Install Only Once"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:303
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:333
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:374
msgid "The configuration of your additional software failed."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:324
msgid ""
"To install it automatically when starting Tails, you can create a persistent "
"storage and activate the <b>Additional Software</b> feature."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:327
msgid "Create Persistent Storage"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:335
msgid "Creating your persistent storage failed."
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:344
#, python-brace-format
msgid "You could install {packages} automatically when starting Tails"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:347
msgid ""
"To do so, you need to run Tails from a USB stick installed using <i>Tails "
"Installer</i>."
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and will be
#. replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:362
#, fuzzy, python-brace-format
msgid "Remove {packages} from your additional software?"
msgstr "Sənin əlaqə proqramın"

#. Translators: Don't translate {packages}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:366
#, python-brace-format
msgid "This will stop installing {packages} automatically."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:368
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:156
msgid "Remove"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:369
#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:119
#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "Ləğv et"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:547
msgid "Installing your additional software from persistent storage..."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:549
msgid "This can take several minutes."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:562
#, fuzzy
msgid "The installation of your additional software failed"
msgstr "Sənin əlaqə proqramın"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:577
msgid "Additional software installed successfully"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:597
msgid "The check for upgrades of your additional software failed"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:599
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:607
#, fuzzy
msgid ""
"Please check your network connection, restart Tails, or read the system log "
"to understand the problem."
msgstr ""
"Təkmilləşmə alınmadı. Bunun səbəbi şəbəkə problemi ola bilər. Lütfən, şəbəkə "
"əlaqəni yoxla, Tails-i yenidən başlatmağa cəhd et, və ya problemi daha yaxşı "
"anlamaq üçün sistem girişini oxu."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:606
#, fuzzy
msgid "The upgrade of your additional software failed"
msgstr "Sənin əlaqə proqramın"

#: config/chroot_local-includes/usr/local/lib/tails-additional-software-notify:39
#, fuzzy
msgid "Documentation"
msgstr "Tails sənədləşməsi"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:96
#, python-brace-format
msgid ""
"Remove {package} from your additional software? This will stop installing "
"the package automatically."
msgstr ""

#. Translators: Don't translate {pkg}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:107
#, python-brace-format
msgid "Failed to remove {pkg}"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:124
#, fuzzy
msgid "Failed to read additional software configuration"
msgstr "Sənin əlaqə proqramın"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:154
#, python-brace-format
msgid "Stop installing {package} automatically"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:180
msgid ""
"To do so, install some software using <a href=\"synaptic.desktop\">Synaptic "
"Package Manager</a> or <a href=\"org.gnome.Terminal.desktop\">APT on the "
"command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:189
msgid ""
"To do so, unlock your persistent storage when starting Tails and install "
"some software using <a href=\"synaptic.desktop\">Synaptic Package Manager</"
"a> or <a href=\"org.gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:199
msgid ""
"To do so, create a persistent storage and install some software using <a "
"href=\"synaptic.desktop\">Synaptic Package Manager</a> or <a href=\"org."
"gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:207
msgid ""
"To do so, install Tails on a USB stick using <a href=\"tails-installer."
"desktop\">Tails Installer</a> and create a persistent storage."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:254
#, fuzzy
msgid "[package not available]"
msgstr "qeyri mümkün"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Sistem saatının sinxronlaşdırılması"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor-un yaxşı işləməsi, xüsusilə də Gizli Xidmətlər üçün dəqiq saata ehtiyacı "
"var. Lütfən, gözlə..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Saatın sinxronlaşdırılması alınmadı!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Tails-in bu versiyasının bilinən təhlükəsizlik problemləri var:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "${nic} şəbəkə kartı deaktivə edilib"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, fuzzy, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"MAC aldadıcısı ${nic_name} (${nic}) şəbəkə kartı üçün alınmadı və müvəqqəti "
"dayandırılmışdır.\n"
"Tails-i yenidən başlatmağı və MAC aldadıcını deaktivə etməyi seçə bilərsən. "
"<a href='file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>Sənədləşmə</a>yə bax."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Bütün şəbəkələşmə deaktivə edilib"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, fuzzy, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"MAC aldadıcısı ${nic_name} (${nic}) şəbəkə kartı üçün alınmadı. Xətanın "
"düzəldilməsi də alınmadığı üçün bütün şəbəkələşmə deaktivə edildi.\n"
"Tails-i yenidən başlatmağı və MAC aldadıcını deaktivə etməyi seçə bilərsən. "
"<a href='file:///usr/share/doc/first_steps/startup_options/mac_spoofing.en."
"html'>Sənədləşmə</a>yə bax."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:110
msgid "Lock Screen"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:125
msgid "Screen Locker"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:131
msgid "Set up a password to unlock the screen."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:149
msgid "Password"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:150
msgid "Confirm"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:35
#, fuzzy
msgid ""
"\"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual\""
msgstr ""
"<b>Təkmilləşmələri yoxlamaq üçün kifayət qədər yaddaş yoxdur.</b>\n"
"\n"
"Tails-in işləməsi üçün sisyemin bütün tələbləri qarşıladığından əmin ol.\n"
"Fayla bax:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Təkmilləşmələrə baxmaq üçün Tails-i yenidən başlatmağa cəhd et.\n"
"\n"
"Ya da manual təkmilləşmə et.\n"
"Bura bax: https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:72
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "xəta:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:73
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Xəta"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Təhlükə: virtual dəzgah müəyyənləşdirilib!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
#, fuzzy
msgid "Warning: non-free virtual machine detected!"
msgstr "Təhlükə: virtual dəzgah müəyyənləşdirilib!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:77
#, fuzzy
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Tails-də nə etdiyini host əməliyyatlar sistemi və virtualizasiya proqramı "
"görə bilir."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:81
#, fuzzy
msgid "Learn more"
msgstr "Tails haqqında daha ətraflı öyrən"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "Tor hazır deyil"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor hazır deyil. Buna baxmayaraq Tor Brauzeri açılsın?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Tor Brauzerini Başlat"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:40
msgid "Tor"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:55
msgid "Open Onion Circuits"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Buna baxmayaraq Təhlükəli Brauzeri açmaq istəyirsən?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
#, fuzzy
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Təhlükəli Brauzer ilə şəbəkə aktivliyi <b>anonim deyil</b>. Təhlükəli "
"Brauzeri yalnız çox vacib hallarda istifadə et, məsələn, əgər internet "
"əlaqəsi üçün sən qeydiyyatdan keçməli, ya da giriş etməlisənsə."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Təhlükəli Brauzerin açılması..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "Bu bir qədər vaxt ala bilər, lütfən, səbrli ol."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Təhlükəli Brauzerin bağlanması..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Bu bir qədər vaxt ala bilər və ola bilsin o tam bağlanana qədər sən "
"Təhlükəli Brauzeri bağlaya bilməzsən."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Tor-un yenidən başladılması alınmadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Təhlükəli Brauzer"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:91
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Digər Təhlükəli Brauzer hazırda işləyir, ya da təmizlənir. Lütfən, birazdan "
"yenidən sına."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:99
msgid "Failed to setup chroot."
msgstr "Chroot-un quraşdırılması alınmadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
#, fuzzy
msgid "Failed to configure browser."
msgstr "Tor-un yenidən başladılması alınmadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:110
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Nə DHCP vasitəsilə heç bir DNS əldə edilmədi, nə də ŞəbəkəMenecerində manual "
"olaraq konfiqurasiya edilmədi."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:121
#, fuzzy
msgid "Failed to run browser."
msgstr "Tor-un yenidən başladılması alınmadı."

#. Translators: Don't translate {volume_label} or {volume_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:58
#, python-brace-format
msgid "{volume_label} ({volume_size})"
msgstr ""

#. Translators: Don't translate {partition_name} or {partition_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:63
#, python-brace-format
msgid "{partition_name} ({partition_size})"
msgstr ""

#. Translators: Don't translate {volume_size}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:68
#, python-brace-format
msgid "{volume_size} Volume"
msgstr ""

#. Translators: Don't translate {volume_name}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:107
#, python-brace-format
msgid "{volume_name} (Read-Only)"
msgstr ""

#. Translators: Don't translate {partition_name} and {container_path}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:115
#, python-brace-format
msgid "{partition_name} in {container_path}"
msgstr ""

#. Translators: Don't translate {volume_name} and {path_to_file_container},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:122
#, python-brace-format
msgid "{volume_name} – {path_to_file_container}"
msgstr ""

#. Translators: Don't translate {partition_name} and {drive_name}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:128
#, python-brace-format
msgid "{partition_name} on {drive_name}"
msgstr ""

#. Translators: Don't translate {volume_name} and {drive_name},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:135
#, python-brace-format
msgid "{volume_name} – {drive_name}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:222
msgid "Wrong passphrase or parameters"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:224
msgid "Error unlocking volume"
msgstr ""

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:228
#, python-brace-format
msgid ""
"Couldn't unlock volume {volume_name}:\n"
"{error_message}"
msgstr ""

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:330
#, python-brace-format
msgid ""
"Couldn't lock volume {volume_name}:\n"
"{error_message}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:332
msgid "Error locking volume"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:83
msgid "No file containers added"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:98
msgid "No VeraCrypt devices detected"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:40
#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:1
msgid "Unlock VeraCrypt Volumes"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:114
msgid "Container already added"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:115
#, python-format
msgid "The file container %s should already be listed."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:131
msgid "Container opened read-only"
msgstr ""

#. Translators: Don't translate {path}, it's a placeholder  and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:133
#, python-brace-format
msgid ""
"The file container {path} could not be opened with write access. It was "
"opened read-only instead. You will not be able to modify the content of the "
"container.\n"
"{error_message}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:138
msgid "Error opening file"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:160
msgid "Not a VeraCrypt container"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:161
#, python-format
msgid "The file %s does not seem to be a VeraCrypt container."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:163
#, fuzzy
msgid "Failed to add container"
msgstr "Tor-un yenidən başladılması alınmadı."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:164
#, python-format
msgid ""
"Could not add file container %s: Timeout while waiting for loop setup.\n"
"Please try using the <i>Disks</i> application instead."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:209
msgid "Choose File Container"
msgstr ""

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Xəta məlumatını paylaş"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Tails sənədləşməsi"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Tails-i necə istifadə edəcəyini öyrən"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Tails haqqında daha ətraflı öyrən"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Brauzer"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonim Veb Brauzer"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "World Wide Web-də qeyri-anonim axtarış"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Təhlükəli Veb Brauzer"

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:2
msgid "Mount VeraCrypt encrypted file containers and devices"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:1
#, fuzzy
msgid "Additional Software"
msgstr "Sənin əlaqə proqramın"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:2
msgid ""
"Configure the additional software installed from your persistent storage "
"when starting Tails"
msgstr ""

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Tails-in xüsusi quraşdırmaları"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr ""

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:1
#, fuzzy
msgid "Remove an additional software package"
msgstr "Sənin əlaqə proqramın"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:2
msgid ""
"Authentication is required to remove a package from your additional software "
"($(command_line))"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:61
msgid "File Containers"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:80
msgid "_Add"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:86
msgid "Add a file container"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:103
msgid "Partitions and Drives"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:121
msgid ""
"This application is not affiliated with or endorsed by the VeraCrypt project "
"or IDRIX."
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:29
msgid "_Open"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:38
msgid "Lock this volume"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:52
msgid "_Unlock"
msgstr ""

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:61
msgid "Detach this volume"
msgstr ""

#: ../config/chroot_local-includes/usr/local/share/mime/packages/unlock-veracrypt-volumes.xml.in.h:1
msgid "TrueCrypt/VeraCrypt container"
msgstr ""

#~ msgid "OpenPGP encryption applet"
#~ msgstr "OpenPGP şifrələnmə apleti"

#~ msgid "Exit"
#~ msgstr "Çıx"

#~ msgid "About"
#~ msgstr "Haqqında"

#~ msgid "Encrypt Clipboard with _Passphrase"
#~ msgstr "Mübadilə Buferini _Şifrə İfadəsi ilə şifrələ"

#~ msgid "Sign/Encrypt Clipboard with Public _Keys"
#~ msgstr "İctimai _Açarlarla Mübadilə Buferinə/i Daxil Ol/Şifrələ"

#~ msgid "_Decrypt/Verify Clipboard"
#~ msgstr "Mübadilə Buferin/i _Şifrəsini Aç/Təsdiqlə"

#~ msgid "_Manage Keys"
#~ msgstr "Açarları _İdarə et"

#~ msgid "The clipboard does not contain valid input data."
#~ msgstr "Mübadilə buferində düzgün giriş məlumatı yoxdur."

#~ msgid "Unknown Trust"
#~ msgstr "Bilinməyən İnam"

#~ msgid "Marginal Trust"
#~ msgstr "Son İnam"

#~ msgid "Full Trust"
#~ msgstr "Tam İnam"

#~ msgid "Ultimate Trust"
#~ msgstr "Başlıca İnam"

#~ msgid "Name"
#~ msgstr "Name"

#~ msgid "Key ID"
#~ msgstr "Açar ID-si"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Fingerprint:"
#~ msgstr "Barmaq izi:"

#~ msgid "User ID:"
#~ msgid_plural "User IDs:"
#~ msgstr[0] "İstifadəçi ID-si:"
#~ msgstr[1] "İstifadəçi ID-ləri:"

#~ msgid "None (Don't sign)"
#~ msgstr "Heç biri (Giriş etmə)"

#~ msgid "Select recipients:"
#~ msgstr "Qəbul ediciləri seç:"

#~ msgid "Hide recipients"
#~ msgstr "Qəbul ediciləri gizlə"

#~ msgid ""
#~ "Hide the user IDs of all recipients of an encrypted message. Otherwise "
#~ "anyone that sees the encrypted message can see who the recipients are."
#~ msgstr ""
#~ "Şifrələnmiş mesajı qəbul edənlərin hamısının istifadəçi ID-ni gizlə. Əks "
#~ "halda, şifrələnmiş mesajı görən hər hansı şəxs onu qəbul edəni də görə "
#~ "bilər."

#~ msgid "Sign message as:"
#~ msgstr "Mesajı belə imzala:"

#~ msgid "Choose keys"
#~ msgstr "Açarları seç"

#~ msgid "Do you trust these keys?"
#~ msgstr "Bu açarlara inanırsan?"

#~ msgid "The following selected key is not fully trusted:"
#~ msgid_plural "The following selected keys are not fully trusted:"
#~ msgstr[0] "Bu seçilmiş açar tam inamlı deyil:"
#~ msgstr[1] "Bu seçilmiş açarlar tam inamlı deyil:"

#~ msgid "Do you trust this key enough to use it anyway?"
#~ msgid_plural "Do you trust these keys enough to use them anyway?"
#~ msgstr[0] ""
#~ "Heç nəyə baxmayaraq bu açarı istifadə edəcək qədər ona inanırsan?"
#~ msgstr[1] ""
#~ "Heç nəyə baxmayaraq bu açarları istifadə edəcək qədər onlara inanırsan?"

#~ msgid "No keys selected"
#~ msgstr "Açar seçilməyib"

#~ msgid ""
#~ "You must select a private key to sign the message, or some public keys to "
#~ "encrypt the message, or both."
#~ msgstr ""
#~ "Mesajı imzalamaq üçün şəxsi açar, şifrələmək üçün hər hansı ictimai açar, "
#~ "ya da hər ikisini seçməlisən."

#~ msgid "No keys available"
#~ msgstr "Açar yoxdur"

#~ msgid ""
#~ "You need a private key to sign messages or a public key to encrypt "
#~ "messages."
#~ msgstr ""
#~ "Mesajı imzalamaq üçün şəxsi, şifrələmək üçün isə ictimai açara ehtiyacın "
#~ "var."

#~ msgid "GnuPG error"
#~ msgstr "GnuPG xətası"

#~ msgid "Therefore the operation cannot be performed."
#~ msgstr "Buna görə də əməliyyat yerinə yetirilə bilmir."

#~ msgid "GnuPG results"
#~ msgstr "GnuPG nəticələr"

#~ msgid "Output of GnuPG:"
#~ msgstr "GnuPG nəticəsi:"

#~ msgid "Other messages provided by GnuPG:"
#~ msgstr "GnuPG tərəfindən təqdim edilmiş başqa mesajlar:"

#~ msgid "Shutdown Immediately"
#~ msgstr "Dərhal Bağlamaq"

#~ msgid "Reboot Immediately"
#~ msgstr "Dərhal Yenidən Yüklənsin"

#~ msgid "The upgrade was successful."
#~ msgstr "Təkmilləşmə uğurla bitdi."

#~ msgid "Network connection blocked?"
#~ msgstr "Şəbəkə əlaqəsi kilidlidir?"

#~ msgid ""
#~ "It looks like you are blocked from the network. This may be related to "
#~ "the MAC spoofing feature. For more information, see the <a href=\\"
#~ "\"file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
#~ "mac_spoofing.en.html#blocked\\\">MAC spoofing documentation</a>."
#~ msgstr ""
#~ "Görünür, sən şəbəkə tərəfindən kilidlənmisən. Bu MAC-ın aldatma "
#~ "funksiyasından da ola bilər. Daha ətraflı məlumat üçün <a href=\\"
#~ "\"file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
#~ "mac_spoofing.en.html#blocked\\\">MAC aldatma sənədləşməsi</a>nə bax."

#~ msgid ""
#~ "<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
#~ "virtualization.en.html'>Learn more...</a>"
#~ msgstr ""
#~ "<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
#~ "virtualization.en.html'>Daha ətraflı...</a>"

#~ msgid "I2P failed to start"
#~ msgstr "I2P başlaya bilmədi"

#~ msgid ""
#~ "Something went wrong when I2P was starting. Check the logs in /var/log/"
#~ "i2p for more information."
#~ msgstr ""
#~ "I2P başlayan zaman nə isə səhv oldu. Daha çox məlumat üçün /var/log/i2p "
#~ "yazılarını oxu."

#~ msgid "I2P's router console is ready"
#~ msgstr "I2P istiqamətləndirici konsol hazırdır"

#~ msgid "You can now access I2P's router console on http://127.0.0.1:7657."
#~ msgstr ""
#~ "İndi I2O istiqamətləndirici konsoluna burada baxa bilərsən: "
#~ "http://127.0.0.1:7657"

#~ msgid "I2P is not ready"
#~ msgstr "I2P hazır deyil"

#~ msgid ""
#~ "Eepsite tunnel not built within six minutes. Check the router console at "
#~ "http://127.0.0.1:7657/logs or the logs in /var/log/i2p for more "
#~ "information. Reconnect to the network to try again."
#~ msgstr ""
#~ "Eepsite tuneli altı dəqiqədə qurulmur. İstiqamətləndirici konsola "
#~ "http://127.0.0.1:7657/logs linkində, daha ətraflı məlumata isə /var/log/"
#~ "i2p girişində baxa bilərsən. Şəbəkəyə yenidən qoşul və yenidən sına."

#~ msgid "I2P is ready"
#~ msgstr "I2P hazırdır"

#~ msgid "You can now access services on I2P."
#~ msgstr "İndi I2P xidmətlərinə daxil ola bilərsən."

#~ msgid "Anonymous overlay network browser"
#~ msgstr "Anonim şəbəkə brauzer örtüyü"

#~ msgid "I2P Browser"
#~ msgstr "I2P Brauzeri"

#~ msgid "Reboot"
#~ msgstr "Yenidən başlat"

#~ msgid "Immediately reboot computer"
#~ msgstr "Kompüteri dərhal yenidən başlat"

#~ msgid "Immediately shut down computer"
#~ msgstr "Kömpüteri dərhal bağla"
