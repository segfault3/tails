# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Adam Stachowicz <saibamenppl@gmail.com>, 2015
# Aron <aron.plotnikowski@cryptolab.net>, 2014
# Dawid Job <hoek@tuta.io>, 2016-2017
# Dawid Job <hoek@tuta.io>, 2014
# Dawid Job <hoek@tuta.io>, 2019
# Dawid Potocki <dpot@disroot.org>, 2019
# Emma Peel, 2019
# Filip <filipiczesio@vp.pl>, 2018
# Dawid Job <hoek@tuta.io>, 2018
# Dawid Job <hoek@tuta.io>, 2014
# Tomasz Ciborski <zenji.yamada.nihon@gmail.com>, 2015-2016
# Marcin S <dzidek1003@o2.pl>, 2018
# oirpos <kuba2707@gmail.com>, 2015
# phla47 <phla47@gmail.com>, 2013
# sebx, 2013-2015
# sebx, 2015
# Waldemar Stoczkowski, 2019
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-30 18:53+0200\n"
"PO-Revision-Date: 2019-07-03 21:57+0000\n"
"Last-Translator: Dawid Potocki <dpot@disroot.org>\n"
"Language-Team: Polish (http://www.transifex.com/otf/torproject/language/"
"pl/)\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n"
"%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n"
"%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor jest gotowy"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Teraz możesz uzyskać dostęp do Internetu."

#: config/chroot_local-includes/etc/whisperback/config.py:69
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Pomóż nam naprawić błąd!</h1>\n"
"<p>Przeczytaj <a href=\"%s\">instrukcje zgłaszania błędów</a>.</p>\n"
"<p><strong>Nie podawaj więcej prywatnych informacji o sobie niż to konieczne!"
"</strong></p>\n"
"<h2>Podanie swojego adresu email</h2>\n"
"<p>\n"
"Poprzez podanie swojego adresu email dajesz nam szanse na skontaktowania się "
"z Tobą i klaryfikację problemu który napotkałeś. \n"
"To jest potrzebne w większości przypadków zgłaszania błędów które "
"otrzymujemy. \n"
"Bez jakichkolwiek danych kontaktowych zgłaszane błędy są bezużyteczne. \n"
"Aczkolwiek, podanie swojego adresu email daje możliwość na podsłuch, np. "
"Twój dostawca email, lub dostawca internetu będzie wiedział, że używasz "
"Tails.\n"
"</p>\n"
"\n"

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:51
msgid ""
"You can install additional software automatically from your persistent "
"storage when starting Tails."
msgstr ""
"Możesz automatycznie instalować dodatkowe oprogramowanie z pamięci stałej "
"przy włączaniu Tails."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:77
msgid ""
"The following software is installed automatically from your persistent "
"storage when starting Tails."
msgstr ""
"Poniższe oprogramowanie jest instalowane automatycznie z twojej pamięci "
"stałej przy starcie Tails."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:135
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:173
msgid ""
"To add more, install some software using <a href=\"synaptic.desktop"
"\">Synaptic Package Manager</a> or <a href=\"org.gnome.Terminal.desktop"
"\">APT on the command line</a>."
msgstr ""
"Aby dodać więcej, zainstaluj oprogramowanie używając <a href=\"synaptic."
"desktop\">Menedżera Paczek Synaptic</a> lub <a href=\"org.gnome.Terminal."
"desktop\">APT w wierszu poleceń</a>."

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:154
msgid "_Create persistent storage"
msgstr "_Stwórz pamięć trwałą"

#: config/chroot_local-includes/usr/local/bin/electrum:57
msgid "Persistence is disabled for Electrum"
msgstr "Zapis plików jest wyłączony dla Electrum"

#: config/chroot_local-includes/usr/local/bin/electrum:59
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"Kiedy zrestartujesz Tails, to wszystkie dane Electrum zostaną utracone, "
"łącznie z Twoim portfelem Bitcoin. Zaleca się, aby uruchomić Electrum tylko "
"wtedy gdy funkcja persistence/zapisu plików jest aktywna."

#: config/chroot_local-includes/usr/local/bin/electrum:60
msgid "Do you want to start Electrum anyway?"
msgstr "Mimo tego, czy chcesz uruchomić Electrum?"

#: config/chroot_local-includes/usr/local/bin/electrum:63
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Uruchom"

#: config/chroot_local-includes/usr/local/bin/electrum:64
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Wyjście"

#: config/chroot_local-includes/usr/local/bin/keepassx:17
#, sh-format
msgid ""
"<b><big>Do you want to rename your <i>KeePassX</i> database?</big></b>\n"
"\n"
"You have a <i>KeePassX</i> database in your <i>Persistent</i> folder:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Renaming it to <i>keepassx.kdbx</i> would allow <i>KeePassX</i> to open it "
"automatically in the future."
msgstr ""
"<b><big>Czy chcesz zmienić nazwę swojej bazy<i>KeePassX</i>?</big></b>\n"
"\n"
"Posiadasz bazę <i>KeePassX</i> w swoim folderze <i>Persistent</i>:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Zmieniając nazwę na <i>keepassx.kdbx</i> umożliwi <i>KeePassX</i> otworzyć "
"ją automatycznie w przyszłości."

#: config/chroot_local-includes/usr/local/bin/keepassx:25
msgid "Rename"
msgstr "Zmień Nazwę"

#: config/chroot_local-includes/usr/local/bin/keepassx:26
msgid "Keep current name"
msgstr "Zachowaj aktualną nazwę"

#: config/chroot_local-includes/usr/local/bin/replace-su-with-sudo:21
msgid "su is disabled. Please use sudo instead."
msgstr "su jest wyłączony. Zamiast tego użyj sudo."

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:75
msgid "Lock screen"
msgstr "Ekran Blokady"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:79
msgid "Suspend"
msgstr "Wstrzymaj"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:83
msgid "Restart"
msgstr "Restart"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:87
msgid "Power Off"
msgstr "Wyłącz"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "O systemie Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "The Amnesic Incognito Live System"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Informacja o wersji:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "niedostępne"

#. Translators: Don't translate {details}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:150
#, python-brace-format
msgid ""
"{details} Please check your list of additional software or read the system "
"log to understand the problem."
msgstr ""
"{details} Proszę o sprawdzenie swojej listy dodatkowego oprogramowania lub "
"przeczytaj systemowy log żeby zrozumieć problem."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:155
msgid ""
"Please check your list of additional software or read the system log to "
"understand the problem."
msgstr ""
"Proszę o sprawdzenie swojej listy dodatkowego oprogramowania lub przeczytaj "
"systemowy log żeby zrozumieć problem. "

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:159
msgid "Show Log"
msgstr "Pokaż log"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:159
msgid "Configure"
msgstr "Konfiguruj"

#. Translators: Don't translate {beginning} or {last}, they are
#. placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:225
#, python-brace-format
msgid "{beginning} and {last}"
msgstr "{beginning} i {last}"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:226
msgid ", "
msgstr ", "

#. Translators: Don't translate {packages}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:292
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:322
#, python-brace-format
msgid "Add {packages} to your additional software?"
msgstr "Dodać {packages} do twojego dodatkowego oprogramowania?"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:294
msgid ""
"To install it automatically from your persistent storage when starting Tails."
msgstr ""
"Aby zainstalować to automatycznie z twojej pamięci trwałej przy starcie "
"Tails."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:296
msgid "Install Every Time"
msgstr "Instaluj za każdym razem"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:297
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:328
msgid "Install Only Once"
msgstr "Zainstaluj tylko raz"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:303
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:333
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:374
msgid "The configuration of your additional software failed."
msgstr "Konfiguracja dodatkowego oprogramowania nie powiodła się."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:324
msgid ""
"To install it automatically when starting Tails, you can create a persistent "
"storage and activate the <b>Additional Software</b> feature."
msgstr ""
"Aby zainstalować to automatycznie przy starcie Tails, możesz utworzyć trwałą "
"pamięć i aktywować funkcję <b>Dodatkowe Oprogramowanie</b>."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:327
msgid "Create Persistent Storage"
msgstr "Utwórz pamięć trwałą"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:335
msgid "Creating your persistent storage failed."
msgstr "Tworzenie twojej pamięci trwałej nie powidło się."

#. Translators: Don't translate {packages}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:344
#, python-brace-format
msgid "You could install {packages} automatically when starting Tails"
msgstr "Możesz instalować {packages} automatycznie przy starcie Tails"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:347
msgid ""
"To do so, you need to run Tails from a USB stick installed using <i>Tails "
"Installer</i>."
msgstr ""
"Aby to zrobić, musisz uruchomić Tails z pamięci USB zainstalowanej używając "
"<i>Instaler Tails</i>."

#. Translators: Don't translate {packages}, it's a placeholder and will be
#. replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:362
#, python-brace-format
msgid "Remove {packages} from your additional software?"
msgstr "Usunąć {packages} z twojego oprogramowania dodatkowego?"

#. Translators: Don't translate {packages}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:366
#, python-brace-format
msgid "This will stop installing {packages} automatically."
msgstr "Zatrzyma to automatyczne instalowanie {packages}."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:368
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:156
msgid "Remove"
msgstr "Usuń"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:369
#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:119
#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "Anuluj"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:547
msgid "Installing your additional software from persistent storage..."
msgstr "Instalowanie twojego dodatkowego oprogramowania z pamięci trwałej..."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:549
msgid "This can take several minutes."
msgstr "Może to zająć kilka minut."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:562
msgid "The installation of your additional software failed"
msgstr "Instalacja twojego dodatkowego oprogramowania nie powiodła się."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:577
msgid "Additional software installed successfully"
msgstr "Oprogramowanie dodatkowe zainstalowano z powodzeniem"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:597
msgid "The check for upgrades of your additional software failed"
msgstr ""
"Sprawdzanie aktualizacji twojego dodatkowego oprogramowania nie powiodło się"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:599
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:607
msgid ""
"Please check your network connection, restart Tails, or read the system log "
"to understand the problem."
msgstr ""
"Proszę sprawdzić swoje połączenie sieciowe, zrestartowanie Tails lub "
"przeczytanie logu systemowego aby zrozumieć problem."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:606
msgid "The upgrade of your additional software failed"
msgstr "Aktualizacja twojego dodatkowego oprogramowania nie powiodła się"

#: config/chroot_local-includes/usr/local/lib/tails-additional-software-notify:39
msgid "Documentation"
msgstr "Dokumentacja"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:96
#, python-brace-format
msgid ""
"Remove {package} from your additional software? This will stop installing "
"the package automatically."
msgstr ""
"Usunąć  {package} z twojego dodatkowego oprogramowania? To zatrzyma "
"automatyczną instalację paczki."

#. Translators: Don't translate {pkg}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:107
#, python-brace-format
msgid "Failed to remove {pkg}"
msgstr "Usunięcie {pkg} nie powiodło się"

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:124
msgid "Failed to read additional software configuration"
msgstr "Nie udało się odczytać dodatkowej konfiguracji oprogramowania"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:154
#, python-brace-format
msgid "Stop installing {package} automatically"
msgstr "Przestań instalować {package} automatycznie"

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:180
msgid ""
"To do so, install some software using <a href=\"synaptic.desktop\">Synaptic "
"Package Manager</a> or <a href=\"org.gnome.Terminal.desktop\">APT on the "
"command line</a>."
msgstr ""
"Aby to zrobić, zainstaluj oprogramowanie używając <a href=\"synaptic.desktop"
"\">Menedżera Paczek Synaptic</a> lub <a href=\"org.gnome.Terminal.desktop"
"\">APT w wierszu poleceń</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:189
msgid ""
"To do so, unlock your persistent storage when starting Tails and install "
"some software using <a href=\"synaptic.desktop\">Synaptic Package Manager</"
"a> or <a href=\"org.gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""
"Aby to zrobić, odblokuj swoją pamięć trwałą przy startowaniu Tails oraz  "
"zainstaluj oprogramowanie używając <a href=\"synaptic.desktop\">Menedżera "
"Paczek Synaptic</a> lub <a href=\"org.gnome.Terminal.desktop\">APT w wierszu "
"poleceń</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:199
msgid ""
"To do so, create a persistent storage and install some software using <a "
"href=\"synaptic.desktop\">Synaptic Package Manager</a> or <a href=\"org."
"gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""
"Aby to zrobić, utwórz pamięć trwałą oraz  zainstaluj oprogramowanie używając "
"<a href=\"synaptic.desktop\">Menedżera Paczek Synaptic</a> lub <a href=\"org."
"gnome.Terminal.desktop\">APT w wierszu poleceń</a>."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:207
msgid ""
"To do so, install Tails on a USB stick using <a href=\"tails-installer."
"desktop\">Tails Installer</a> and create a persistent storage."
msgstr ""
"Aby to zrobić, zainstaluj Tails na pamięci USB używając <a href=\"tails-"
"installer.desktop\">Instaler Tails</a> oraz utwórz pamięć trwałą."

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:254
msgid "[package not available]"
msgstr "[paczka niedostępna]"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Synchronizacja zegara systemowego"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor wymaga dokładnego czasu do poprawnego działania, szczególnie w przypadku "
"Ukrytych Serwisów. Proszę czekać..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Nie udało się zsynchronizować zegara!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Ta wersja Tails ma błędy bezpieczeństwa:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr "Znane problemy bezpieczeństwa"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Karta sieciowa ${nic} wyłączona"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Spoofing MAC dla karty sieciowej ${nic_name} (${nic}) nie udał się, a zatem "
"jest ona tymczasowo nieaktywna. Spróbuj uruchomić system Tails ponownie i "
"wyłączyć spoofing MAC."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Wszystkie połączenia wyłączone"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Spoofing MAC dla karty sieciowej ${nic_name} (${nic}) nie udał się. Próba "
"usunięcia błędu także się nie powiodła, a zatem wszystkie połączenia "
"sieciowe pozostają nieaktywne. Spróbuj uruchomić system Tails ponownie i "
"wyłączyć spoofing MAC."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:110
msgid "Lock Screen"
msgstr "Ekran blokady"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:125
msgid "Screen Locker"
msgstr "Blokada ekranu"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:131
msgid "Set up a password to unlock the screen."
msgstr "Ustaw hasło aby odblokować ekran."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:149
msgid "Password"
msgstr "Hasło"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:150
msgid "Confirm"
msgstr "Potwierdź"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:35
msgid ""
"\"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual\""
msgstr ""
"\"<b>Brak wystarczającej ilości dostępnej pamięci aby sprawdzić aktualizacje."
"</b>\n"
"\n"
"Upewnij się, że ten system spełnia wymagania do uruchamiania Tails.\n"
"Zobacz file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Spróbuj zrestartować Tails aby sprawdzić aktualizacje ponownie.\n"
"\n"
"Lub wykonaj manualną aktualizację.\n"
"Patrz https://tails.boum.org/doc/first_steps/upgrade#manual\""

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:72
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "błąd:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:73
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Błąd"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Ostrzeżenie: wykryto maszynę wirtualną!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
msgid "Warning: non-free virtual machine detected!"
msgstr "Ostrzeżenie: wykryto niewolne oprogramowanie maszyny wirtualnej!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:77
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Zarówno system operacyjny hosta, jak i oprogramowanie wirtualizacji "
"software'u są w stanie monitorować Twoją aktywność w systemie Tails. W "
"odniesieniu do nich, wiarygodne jest wyłącznie oprogramowanie wolne."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:81
msgid "Learn more"
msgstr "Dowiedz się więcej"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "Tor nie jest gotowy"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor nie jest gotowy. Uruchomić Tor Browser mimo tego?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Start Tor Browser"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:40
msgid "Tor"
msgstr "Tor"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:55
msgid "Open Onion Circuits"
msgstr "Otwarte Obwody Onion"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Czy jesteś pewien, że chcesz uruchomić Niebezpieczną Przeglądarkę?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Aktywność sieciowa poprzez Niebezpieczną Przeglądarkę <b>nie jest anonimowa</"
"b>\\nWykorzystuj Niebezpieczną Przeglądarkę tylko wtedy, gdy jest to "
"potrzebne, na przykład podczas logowania się lub rejestrowania celem "
"aktywacji połączenia z internetem."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Uruchamianie Niebezpiecznej Przeglądarki..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "To może chwilę potrwać, prosimy o cierpliwość."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Wyłączanie Niebezpiecznej Przeglądarki..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"To może chwilę potrwać. Nie restartuj Niebezpiecznej Przeglądarki dopóki nie "
"zostanie ona poprawnie wyłączona."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Nie udało się zrestartować Tor'a."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Niebezpieczna Przeglądarka"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:91
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Niebezpieczna Przeglądarka jest już uruchomiona albo aktualnie czyszczona. "
"Proszę spróbuj ponownie za chwilę."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:99
msgid "Failed to setup chroot."
msgstr "Nie udało się skonfigurować chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
msgid "Failed to configure browser."
msgstr "Nie udało skonfigurować się przeglądarki."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:110
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Nie otrzymano żadnego serwera DNS poprzez DHCP ani z ręcznej konfiguracji w "
"NetworkManager."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:121
msgid "Failed to run browser."
msgstr "Nie udało uruchomić się przeglądarki."

#. Translators: Don't translate {volume_label} or {volume_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:58
#, python-brace-format
msgid "{volume_label} ({volume_size})"
msgstr "{volume_label} ({volume_size})"

#. Translators: Don't translate {partition_name} or {partition_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:63
#, python-brace-format
msgid "{partition_name} ({partition_size})"
msgstr "{partition_name} ({partition_size})"

#. Translators: Don't translate {volume_size}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:68
#, python-brace-format
msgid "{volume_size} Volume"
msgstr "{volume_size} Wielkość"

#. Translators: Don't translate {volume_name}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:107
#, python-brace-format
msgid "{volume_name} (Read-Only)"
msgstr "{volume_name} (Tylko do odczytu)"

#. Translators: Don't translate {partition_name} and {container_path}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:115
#, python-brace-format
msgid "{partition_name} in {container_path}"
msgstr "{partition_name} w {container_path}"

#. Translators: Don't translate {volume_name} and {path_to_file_container},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:122
#, python-brace-format
msgid "{volume_name} – {path_to_file_container}"
msgstr "{volume_name} – {path_to_file_container}"

#. Translators: Don't translate {partition_name} and {drive_name}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:128
#, python-brace-format
msgid "{partition_name} on {drive_name}"
msgstr "{partition_name} na {drive_name}"

#. Translators: Don't translate {volume_name} and {drive_name},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:135
#, python-brace-format
msgid "{volume_name} – {drive_name}"
msgstr "{volume_name} – {drive_name}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:222
msgid "Wrong passphrase or parameters"
msgstr "Nieprawidłowe hasło lub parametry"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:224
msgid "Error unlocking volume"
msgstr "Błąd odblokowywania woluminu"

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:228
#, python-brace-format
msgid ""
"Couldn't unlock volume {volume_name}:\n"
"{error_message}"
msgstr ""
"Nie można odblokować woluminu {volume_name}:\n"
"{error_message}"

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:330
#, python-brace-format
msgid ""
"Couldn't lock volume {volume_name}:\n"
"{error_message}"
msgstr ""
"Nie można zablokować woluminu {volume_name}:\n"
"{error_message}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:332
msgid "Error locking volume"
msgstr "Błąd blokowania woluminu"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:83
msgid "No file containers added"
msgstr "Brak dodanych kontenerów plików"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:98
msgid "No VeraCrypt devices detected"
msgstr "Brak wykrytych urządzeń VeraCrypt"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:40
#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:1
msgid "Unlock VeraCrypt Volumes"
msgstr "Oblokuj woluminy VeraCrypt"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:114
msgid "Container already added"
msgstr "Kontener już jest dodany"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:115
#, python-format
msgid "The file container %s should already be listed."
msgstr "Kontener plików %s powinien już być umieszczony w wykazie."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:131
msgid "Container opened read-only"
msgstr "Kontener otwarty tylko do odczytu"

#. Translators: Don't translate {path}, it's a placeholder  and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:133
#, python-brace-format
msgid ""
"The file container {path} could not be opened with write access. It was "
"opened read-only instead. You will not be able to modify the content of the "
"container.\n"
"{error_message}"
msgstr ""
"Kontener plików {path} nie mógł być otwarty z prawami zapisu. Zamiast tego "
"został otwarty tylko do odczytu. Nie będziesz mógł modyfikować zawartości "
"tego kontenera.\n"
"{error_message}"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:138
msgid "Error opening file"
msgstr "Błąd podczas otwierania pliku"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:160
msgid "Not a VeraCrypt container"
msgstr "Nie jest to kontener VeraCrypt"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:161
#, python-format
msgid "The file %s does not seem to be a VeraCrypt container."
msgstr "Plik %s nie wydaje się być kontenerem VeraCrypt."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:163
msgid "Failed to add container"
msgstr "Nie udało się dodać konteneru"

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:164
#, python-format
msgid ""
"Could not add file container %s: Timeout while waiting for loop setup.\n"
"Please try using the <i>Disks</i> application instead."
msgstr ""
"Nie można dodać kontenera plików %s: Limit czasu podczas oczekiwania na "
"konfigurację pętli.\n"
"Spróbuj zamiast tego użyć aplikacji <i>Dyski</i>."

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:209
msgid "Choose File Container"
msgstr "Wybierz kontener plików"

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Zgłoś błąd"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Dokumentacja Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Dowiedz się jak używać Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Dowiedz się więcej o Tails"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Browser"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonimowa Przeglądarka"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Przeglądaj Internet bez anonimowości"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Niebezpieczna Przeglądarka"

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:2
msgid "Mount VeraCrypt encrypted file containers and devices"
msgstr "Zamontuj zaszyfrowane kontenery plików oraz urządzenia VeraCrypt"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:1
msgid "Additional Software"
msgstr "Dodatkowe oprogramowanie"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:2
msgid ""
"Configure the additional software installed from your persistent storage "
"when starting Tails"
msgstr ""
"Konfiguruj dodatkowe oprogramowanie zainstalowane z pamięci trwałej przy "
"stracie Tails"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Narzędzia Tails"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr "Aby uruchomić terminal root, musisz się uwierzytelnić."

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:1
msgid "Remove an additional software package"
msgstr "Usuń dodatkową paczkę oprogramowania"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:2
msgid ""
"Authentication is required to remove a package from your additional software "
"($(command_line))"
msgstr ""
"Aby usunąć paczkę z twojego dodatkiwego oprogramowania ($(command_line)) "
"wymagana jest autentykacja"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:61
msgid "File Containers"
msgstr "Kontenery plików"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:80
msgid "_Add"
msgstr "_Dodaj"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:86
msgid "Add a file container"
msgstr "Dodaj kontener plików"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:103
msgid "Partitions and Drives"
msgstr "Partycje oraz Dyski"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/main.ui.in:121
msgid ""
"This application is not affiliated with or endorsed by the VeraCrypt project "
"or IDRIX."
msgstr ""
"Ta aplikacja nie jest stowarzyszona z, ani popierana przez projekt VeraCrypt "
"ani IDRIX."

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:29
msgid "_Open"
msgstr "_Otwarte"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:38
msgid "Lock this volume"
msgstr "Zablokuj ten wolumin"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:52
msgid "_Unlock"
msgstr "_Odblokowanie"

#: ../config/chroot_local-includes/usr/share/tails/unlock-veracrypt-volumes/volume.ui.in:61
msgid "Detach this volume"
msgstr "Odłącz ten wolumin"

#: ../config/chroot_local-includes/usr/local/share/mime/packages/unlock-veracrypt-volumes.xml.in.h:1
msgid "TrueCrypt/VeraCrypt container"
msgstr "Kontener TrueCrypt/VeraCrypt"
